import re


def get_file_content_by_path(path):
    with open(path) as f:
        return f.read()


def check_if_uri_is_right(uri):
    uri_pattern = r'\/texts\/([ЁёА-я]|\w)+.txt'
    return bool(re.search(uri_pattern, uri))
