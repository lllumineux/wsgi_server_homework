from helpers import get_file_content_by_path, check_if_uri_is_right

from urllib.parse import unquote


RESPONSE_STATUSES = {
    200: '200 OK',
    404: '404 Not Found',
    405: '405 Not Allowed'
}


def application(env, start_response):
    decoded_url = unquote(env['REQUEST_URI'])

    response_headers = [('Content-Type', 'text/plain; charset=UTF-8')]
    response_body = []

    if not check_if_uri_is_right(decoded_url):
        start_response(RESPONSE_STATUSES[405], response_headers)
        return response_body

    try:
        file_content = get_file_content_by_path(decoded_url[1:])
        response_body.append(bytes(file_content, 'utf8'))
        start_response(RESPONSE_STATUSES[200], response_headers)

    except FileNotFoundError:
        start_response(RESPONSE_STATUSES[404], response_headers)

    return response_body
