import helpers


def test_true_1():
    assert helpers.check_if_uri_is_right('/texts/test.txt') is True


def test_true_2():
    assert helpers.check_if_uri_is_right('/texts/фамилия_имя.txt') is True


def test_false_1():
    assert helpers.check_if_uri_is_right('/texts/test.py') is False


def test_false_2():
    assert helpers.check_if_uri_is_right('/files_generator.py') is False
