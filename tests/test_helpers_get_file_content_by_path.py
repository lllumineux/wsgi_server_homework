import os

import pytest

import helpers


def test_file_exists():
    with open('./texts/test.txt', mode='w+') as f:
        f.write('*test file content*')
    assert helpers.get_file_content_by_path('texts/test.txt') == '*test file content*'


def test_file_missing():
    with pytest.raises(FileNotFoundError):
        os.remove('./texts/test.txt')
        helpers.get_file_content_by_path('texts/test.txt')
