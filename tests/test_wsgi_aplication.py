import os

import wsgi


class StartResponseMockup:
    def __init__(self):
        self.response_status = ''
        self.response_headers = []

    def __call__(self, status, headers):
        self.response_status = status
        self.response_headers = headers


def test_200_ok():
    with open('./texts/test.txt', mode='w+') as f:
        f.write('*test file content*')
    start_response_mockup = StartResponseMockup()
    response_body = wsgi.application({'REQUEST_URI': '/texts/test.txt'}, start_response_mockup)
    assert (
            start_response_mockup.response_status == '200 OK'
            and start_response_mockup.response_headers == [('Content-Type', 'text/plain')]
            and response_body == [b'*test file content*']
    )


def test_404_not_found():
    os.remove('./texts/test.txt')
    start_response_mockup = StartResponseMockup()
    response_body = wsgi.application({'REQUEST_URI': '/texts/test.txt'}, start_response_mockup)
    assert (
            start_response_mockup.response_status == '404 Not Found'
            and start_response_mockup.response_headers == [('Content-Type', 'text/plain')]
            and response_body == []
    )


def test_405_not_allowed():
    start_response_mockup = StartResponseMockup()
    response_body = wsgi.application({'REQUEST_URI': 'wsgi.py'}, start_response_mockup)
    assert (
            start_response_mockup.response_status == '405 Not Allowed'
            and start_response_mockup.response_headers == [('Content-Type', 'text/plain')]
            and response_body == []
    )
